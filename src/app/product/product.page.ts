import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalApiService } from '../services/local-api-service.service';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {
product = {};
catalog:any;
  constructor(
    private activateRoute: ActivatedRoute,
    private localApiService: LocalApiService,
    private cartService: CartService,
    private router: Router
  ) { }

  ngOnInit() {
    const id = +this.activateRoute.snapshot.params.id;
    const idCat = +this.activateRoute.snapshot.paramMap.get('idCat');
    this.localApiService.get_catalogs().subscribe(catalogs => {
      this.catalog = catalogs[idCat];
      this.product = this.catalog.items[id];
      console.log(this.product);
    });
  }

}
