import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { CartService } from '../services/cart.service';
import { LocalApiService } from '../services/local-api-service.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styleUrls: ['./category.page.scss'],
})
export class CategoryPage implements OnInit {
  [x: string]: any;
  catId;
  catalog = {};
  cart = [];
  constructor(
    private activateRoute: ActivatedRoute,
    private localApiService: LocalApiService,
    private cartService: CartService,
    private router: Router
  ) {}
  ngOnInit() {
    const id = +this.activateRoute.snapshot.params.id;
    this.localApiService.get_catalogs().subscribe(catalogs => {
      this.catalog = catalogs[id];
      this.catId = id;
    });
    this.cart = this.cartService.getCart();
  }
  addToCart(product) {
    this.cartService.addProduct(product);
    console.log(this.cart)
  }
  openCart() {
    this.router.navigate(['cart']);
  }
  openProduct(id:number){
    this.router.navigate(['product',this.catId,id]);
    }

}


