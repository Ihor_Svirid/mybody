import { CartService } from './../services/cart.service';
import { Component, OnInit } from '@angular/core';
import { splitClasses } from '@angular/compiler';
 
@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
 
  selectedItems = [];
  id ;
  total = 0;
  items = this.cartService.getCart();
 
  constructor(private cartService: CartService) { }
 
  ngOnInit() {
    // this.items = this.cartService.getCart();
    let selected = {};
    for (let obj of this.items) {
      if (selected[obj.id]) {
        selected[obj.id].count++;
      } else {
        selected[obj.id] = {...obj, count: 1};
      }
    }
    this.selectedItems = Object.keys(selected).map(key => selected[key])
    this.total = this.selectedItems.reduce((a, b) => a + (b.count * b.price), 0);
    console.log(this.selectedItems)
    
  }
  delItem(index){
      console.log(index);
      this.id =this.selectedItems[index].id;
      this.cartService.delFromArr(this.id);
      this.selectedItems.splice(index,1);
      this.items = this.cartService.getCart();
      this.total = this.selectedItems.reduce((a, b) => a + (b.count * b.price), 0);
  }
  
  
}