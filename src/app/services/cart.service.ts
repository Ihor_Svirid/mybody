import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private cart = [];
  length;
  constructor() { }
  getCart() {
    return this.cart;
  }
  addProduct(product) {
    this.cart.push(product);
  }
  delFromArr(id){
    this.cart =this.cart.filter((item) => item.id !== id);
  }

}
