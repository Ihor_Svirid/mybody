import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocalApiService {
  private baseUrl = 'http://localhost:3000';
  constructor(private httpClient: HttpClient) {}
  get<T>(path: string): Observable<T> {
    // TODO перехват коли і-нету нема + помилка парсеру + сервер упав
    return this.httpClient.get<T>(this.baseUrl + path);
  }
  get_news<T>()  {
    return this.get<T>('/news');
  }
  get_catalogs<T>() {
    return this.get<T>('/catalogs');
  }
}
