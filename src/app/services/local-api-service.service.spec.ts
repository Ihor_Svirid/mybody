import { TestBed } from '@angular/core/testing';

import { LocalApiServiceService } from './local-api-service.service';

describe('LocalApiServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LocalApiServiceService = TestBed.get(LocalApiServiceService);
    expect(service).toBeTruthy();
  });
});
