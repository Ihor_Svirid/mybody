import { Component } from '@angular/core';
import { Router,NavigationExtras } from '@angular/router';
import {LocalApiService} from '../services/local-api-service.service';
import { CartService } from '../services/cart.service';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'], 
  providers: [LocalApiService]
})
export class Tab1Page {

  products;
  cart = [];

  constructor(private router: Router,private localApiService:LocalApiService, private cartService: CartService) {
  this.localApiService.get_catalogs().subscribe(
    data =>{
      this.products=data;
      console.log(this.products);
    }
  )
}
ngOnInit() {
  this.cart = this.cartService.getCart();
}
addToCart(product) {
  this.cartService.addProduct(product);
  console.log(this.cart)
  
}
openCart() {
  this.router.navigate(['cart']);
  this.cart = this.cartService.getCart();
}
openKalk(){
  this.router.navigate(['kalk']);
}
openCategory(id:number){
  this.router.navigate(['category',id]);
  }


}
